import signal
from logging import getLogger
from tornado.ioloop import IOLoop


class OsSignalHandler():
    def __init__(self, log=getLogger('monitor')):
        self.log = log
        self.servers = list()

        # handle interrupts
        signal.signal(signal.SIGINT, self.handle_signal)
        # Handle unix kill command
        signal.signal(signal.SIGTERM, self.handle_signal)

    def add(self, server):
        self.servers.append(server)

    def handle_keyboard_interrupt(self):
        self._beautifully_exit()

    def handle_signal(self, *args):
        self._beautifully_exit()

    def _beautifully_exit(self):
        print("Disconnecting clients")
        IOLoop.instance().stop()