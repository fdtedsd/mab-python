from hashlib import sha256
from random import randint
from datetime import datetime
from math import log2
import json

from tornado.gen import coroutine
from tornado.web import RequestHandler, Application
import tornado.ioloop


from util import OsSignalHandler


class MinerApplication(Application):
    _difficulty_ = (8, 18)
    _recalculate_difficulty = 1000
    _average_block_time = 12
    _blocks = []

    transactions = []

    def __init__(self, *args, **kwargs):
        self.difficulty, _ = self._difficulty_
        super().__init__(*args, **kwargs)

    @coroutine
    def start_mining(self):
        while True:
            self.update_difficulty()

            height = len(self._blocks)
            parent = self._blocks[-1]['hash'] if height> 0 else "Genesis Block"

            block = {
                'height': height,
                'parent': parent,
                'timestamp': int(datetime.now().timestamp()),
                'difficulty': self.difficulty,
                'transactions': [t.decode() for t in self.transactions],
            }

            self.transactions = []

            while not self.valid_block(block):
                yield None
                block['entropy'] = randint(0, 2 ** 64)
                block['hash'] = self.hash_block(block).hex()

            self._blocks.append(block)

            print("[MINED] Block #{0}:{1} [D: {2}] with {3} transactions".format(
                    block['height'], block['hash'], block['difficulty'], len(block['transactions'])
                )
            )

    def valid_block(self, block):
        if 'hash' not in block:
            return False

        h = bytes.fromhex(block['hash'])

        return self.check_hash_difficulty(h, self.difficulty)

    def update_difficulty(self):

        height = len(self._blocks)

        if height > 0 and height % self._recalculate_difficulty == 0:
            end = self._blocks[-1]
            start = self._blocks[-self._recalculate_difficulty]

            average_interval = (int(end['timestamp']) - int(start['timestamp'])) / self._recalculate_difficulty
            step = average_interval / (2**self.difficulty)

            difficulty = int(log2(self._average_block_time / (step + .00000001)))

            _minimum_difficulty, _maximum_difficulty = self._difficulty_

            self.difficulty = min(max(_minimum_difficulty, difficulty), _maximum_difficulty)

            print('[DIFF] Updated difficulty to {0} (m {1}, M {2}, C {3})'.format(
                    self.difficulty, _minimum_difficulty, _maximum_difficulty, difficulty
                )
            )

    @staticmethod
    def hash_block(block):
        m = sha256()
        m.update(str(block['parent']).encode())
        m.update(str(block['timestamp']).encode())
        m.update(str(block['transactions']).encode())
        m.update(str(block['entropy']).encode())
        return m.digest()

    @staticmethod
    def check_hash_difficulty(h, dif):
        bits_in_a_byte = 8

        zero_bytes = int(dif / bits_in_a_byte)
        zero_bits = dif % bits_in_a_byte

        if h.startswith(bytes(zero_bytes)):
            mask = ~(2**bits_in_a_byte-1 >> zero_bytes)
            return zero_bits == 0 or h[zero_bytes] & mask == 0

        return False


class MinerHandler(RequestHandler):
    def post(self):
        data = self.request.body
        self.application.transactions.append(data)

        # print("[INFO] Got a new transaction for {0}".format(str(data)))

    def get(self):
        self.write(
            "<html><body>Blocos:<pre>{0}</pre></body></html>".format(
                json.dumps(self.application._blocks, indent=2)
            )
        )


app = MinerApplication([
    (r"/", MinerHandler)
], debug=True)

app.start_mining()

signal_handler = OsSignalHandler()
signal_handler.add(app)

if __name__ == '__main__':
    print('[INFO] App server started at ' + str(datetime.now()))
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()