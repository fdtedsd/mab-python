Requires Python >= 3.5

Install requirements: pip install -r requirements.txt

Run the single node miner: python single.py

It'll start mining and output the mined hashes

Access http://localhost:8888 and check out the blocks

You may send transaction as post requests to http://localhost:8888 as in:

curl -H "Content-Type: application/json" -X POST -d 'YOUR_DATA_GOES_HERE' http://localhost:8888/

You may also want to fire transactions automatically using:

python test_single.py