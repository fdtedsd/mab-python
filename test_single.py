from time import sleep
from random import randint

from requests import post

while True:
    data = str(randint(0, 2 ** 64))
    r = post('http://localhost:8888', data=data)

    print("[TX] Data: {0}".format(data))

    sleep(randint(0, 5)/10)